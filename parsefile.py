#!/usr/bin/env python3

from csv import reader
import configparser
from influxdb import InfluxDBClient
import json
import argparse
import datetime

parser = argparse.ArgumentParser()
parser = argparse.ArgumentParser(description='Process some arguments.')
parser.add_argument('--file', type=str, required=True)
args = parser.parse_args()

# Get some variables outside this script
config = configparser.ConfigParser()

try:
    f = open("config.cfg", 'rb')
except OSError:
    print("Could not open/read file: config.cfg")
    sys.exit()

with f:
    config.read("config.cfg")
    csv_file = args.file
    influx_host = config['DETAILS']['INFLUXDB_URL']
    db_name = config['DETAILS']['DB_NAME']
    measurement = config['DETAILS']['MEASUREMENT']


client = InfluxDBClient(host=influx_host, port=8086)
client.create_database(db_name)
client.get_list_database()
client.switch_database(db_name)


# open file in read mode
with open(csv_file, 'r', encoding='latin-1') as read_obj:
    # pass the file object to reader() to get the reader object
    csv_reader = reader(read_obj)
    # Check if csv has header and skip parsing that header is not empty
    header = next(csv_reader)
    if header != None:
        # Iterate over each row in the csv using reader object
        for row in csv_reader:
            # row variable is a list that represents a row in csv
            #print(row)
            #print(type(row[6]))
            #print(row[4], float(row[6].replace(',','.')), row[8], row[9])

            uiteindelijkeprijs = 5 * int(row[2])
            json_body = [
    {
        "measurement": measurement,
        "time": row[0] + "T0:00:00Z",
        "fields": {
            "aantal": int(row[2]),
            "uiteindelijkeprijs": float(uiteindelijkeprijs)
        }
    }
]
            jsonStr = json.dumps(json_body)
            print(jsonStr)
            client.write_points(json_body)
