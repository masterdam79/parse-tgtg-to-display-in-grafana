# parse-TooGoodToGo-to-display-in-grafana

A Python script that can be run in a container that iterates over each line in a TooGoodToGo Export file and pushes the data to Grafana

# Setup

1. Install ```influxdb``` using ```pip```
  - ``python3 -m pip install influxdb```
2. Rename ```config``` to ```config.cfg```
  - Adapt values in ```config.cfg``` to match your environment
